# Tools

## 1.python_lambda_packager:
A script to automatically package up a python script with its dependencies into a zip file in preparation for deployment as a lambda function.
## 2.smart_quote_remover:
A script to remove all smart quotes from the file specified in the first command line argument and replace them with the correct quotes.
## 3.allcows:
Prints to the terminal all cowsay cows stored in /usr/local/Cellar/cowsay/3.04/share/cows
##4.docker:
### del_all_proc+images:
Based on a command line argument deletes either the running docker processes, images or both.
* ps:   running processes only
* i:    images only
* all:  images and running processes
* help: prints help

This repo will be periodically updated  with DevOps tools as I/others create them.
