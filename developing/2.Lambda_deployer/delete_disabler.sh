#!/bin/bash

## SCRIPT TO DELETE ALL OBJECTS ASSOCIATED WITH THE RECURRING LAMBDA FUNCTION ##

# Get variables from variables script -----------------------------------------
source variables.sh

# Script ----------------------------------------------------------------------
# remove targets
aws events remove-targets \
--rule $CLOUDWATCH_RULE_NAME \
--ids 1 \
--region $REGION > /dev/null
echo "targets removed from cloudwatch rule"

# delete rule
aws events delete-rule \
--name $CLOUDWATCH_RULE_NAME \
--region $REGION
echo "Rule deleted"

# delete lambda function
aws lambda delete-function \
--function-name $LAMBDA_FUNCTION_NAME \
--region $REGION
echo "Lambda function deleted"

# detach policy from role
aws iam detach-role-policy \
--role-name $LAMBDA_ROLE_NAME \
--policy-arn arn:aws:iam::109964479621:policy/$LAMBDA_ROLE_NAME \
--region $REGION

# delete role
aws iam delete-role \
--role-name $lambda_role_name \
--region $REGION
echo "Role deleted"

# delete policy
aws iam delete-policy \
--policy-arn arn:aws:iam::109964479621:policy/$lambda_policy_name \
--region $REGION
echo "Policy deleted"

echo "if there were no errors then everything has now been deleted, have a nice day."
