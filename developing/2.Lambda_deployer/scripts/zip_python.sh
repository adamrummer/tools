#!/bin/bash
# This script packages up a python file along with its dependencies as specified
# in the requirements.txt file in the same location as the script.
# NB: uses with python 3

# Debugging
# "Verbose" mode, verbose mode is actually -v
# set -x
# stop script if any command returns an error
# set -e

# Parameters -------------------------------------------------------------------
# adds arguments to command
while getopts 'hf:p::d::' opt
do
  case $opt in
    h)
      ACTION=HELP ;;
    f)
      script_to_package=${OPTARG}
      script_to_package=$(echo ${OPTARG} | awk 'BEGIN{FS=OFS="."} NF--') ;;
    p)
      python=${OPTARG} ;; # defaults to most recent version of python
    d) # deploy function
      deploy="true"

  esac
done

# options i'd like to add
# d) dependencies space separated string
# r) path to requirements file

# HELP -------------------------------------------------------------------------

if [[ -z $script_to_package ]] || [[ $ACTION="HELP" ]]; then
  echo '--------------------------------------------------------------------------------'
  echo 'SYNTAX:'
  echo "$(basename $0)"
  echo "-f <path_to_python_script>"
  echo "-p <python_version_2_or_3> (optional)"
  exit 0
fi

# VARIABLES --------------------------------------------------------------------
# directory containing script to be packaged
script_dir=$(dirname $script_to_package)
# script name
script_name=$(basename $script_to_package)
# name of directory to create virtual environment inside
virtual_env_dir=py_virtual_env
# sets python version, defaults to 3.x
if [[ $python =~ 3\.*[0-9]* ]] || [[ -z $python ]]; then
  python=3.x
  # export lambda_runtime=python3.7
elif [[ $python =~ 2\.*[0-9]* ]] then
  python=2.x
  # export lambda_runtime=python2.7
fi
# DEPENDENCIES -----------------------------------------------------------------
# Currently no check for python/pip
# checks whether or not virtualenv_check is installed
virtualenv_check="command -v virtualenv"
if [[ -z $virtualenv_check ]];  then
  if [[ python=3.x ]];  then
    sudo pip3 install -y virtualenv
  elif [[ python=2.x ]];  then
    sudo pip install -y virtualenv
  fi
fi

# SCRIPT -----------------------------------------------------------------------
# Set up virtual environment
chmod +x ${script_name}
echo "Setting up virtual environment."
if [[ python=3.x ]];  then
  virtualenv -p python3 $virtual_env_dir
elif [[ python=2.x ]];  then
  virtualenv $virtual_env_dir
fi

source ${virtual_env_dir}/bin/activate

# Install dependencies for lambda script
# checks the existance of a requirements file
if [[ -f "requirements.txt" ]];  then
  echo "Installing libraries from requirements.txt"
  if [[ python=3.x ]];  then
    pip3 install -r "$script_dir"/requirements.txt
  elif [[ python=2.x ]];  then
    pip install -r "$script_dir"/requirements.txt
  fi
else
  # cleans up after self
  echo "No requirements.txt file found in same directory as script."
  echo "cleaning up..."
  rm -rf $virtual_env_dir
  deactivate
  echo "clean up complete."
  exit 1
fi

# zip up libraries inside virtual environment
echo "zipping up script dependencies"
zip -r9 "$script_name".zip ${virtual_env_dir}/lib/python3.7/site-packages/*
# adds the script to zip file
echo "Adding script into zip file"
zip -g "$script_name".zip ${script_to_package}.py


# Resets environment
echo "Returning to normal environment"
deactivate
rm -rf $virtual_env_dir
echo "zip file created."
